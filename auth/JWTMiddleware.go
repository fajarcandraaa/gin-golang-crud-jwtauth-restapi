package auth

import (
	"fmt"
	db "gin-golang-restapi/helpers"
	"gin-golang-restapi/repository"
	"gin-golang-restapi/repository/middleware"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

func AuthorizeJWT() gin.HandlerFunc {
	db := db.ConnectDatabase()
	userRep := repository.UserRepo(db)
	return func(c *gin.Context) {
		const BEARER_SCHEMA = "Bearer "
		authHeader := c.GetHeader("Authorization")
		tokenString := authHeader[len(BEARER_SCHEMA):]
		token, _ := middleware.JWTAuthService().ValidateToken(tokenString)
		// fmt.Println("log auth 3 :", claim)
		if token.Valid {
			claims := token.Claims.(jwt.MapClaims)
			validatetoken := userRep.ExistToken(claims["name"].(string), tokenString)
			if validatetoken {
				fmt.Println(claims)
			} else {
				c.JSON(http.StatusUnauthorized, gin.H{
					"status":  false,
					"messege": "You can't use this token auth, please re-login to get new token",
				})
				c.AbortWithStatus(http.StatusUnauthorized)
			}
		} else {
			// fmt.Println("log auth 4 :", err)

			c.JSON(http.StatusUnauthorized, gin.H{
				"status":  false,
				"messege": "Something wrong with your token auth",
			})
			c.AbortWithStatus(http.StatusUnauthorized)
		}

	}
}
