package models

import "time"

type Balancetype string
type User struct {
	ID        int
	Username  string `json:"username" binding:"required"`
	Email     string `json:"email"`
	Password  string `json:"password" binding:"required"`
	Token     string
	Detail	[]User_Balance `gorm:"Foreignkey:UserId;association_foreignkey:ID;"`
	CreatedAt time.Time
	UpdatedAt time.Time
}

type User_Balance struct {
	ID             int
	UserId         int    `json:"user_id"`
	Balance        string `json:"balance"`
	BalanceAchieve int    `json:"balance_acheive"`
	History 	[]User_Balance_History `gorm:"Foreignkey:UserBalanceId;association_foreignkey:ID;"`
	CreatedAt      time.Time
	UpdatedAt      time.Time
}

type User_Balance_History struct {
	ID            int
	UserBalanceId int
	BalanceBefore int
	BalanceAfter  int
	Activity      string
	Type          Balancetype
	Ip            string
	Location      string
	UserAgent     string
	Author        string
	TransferTo    string
	Notes         string
	CreatedAt     time.Time
	UpdatedAt     time.Time
}

const (
	DEBIT  Balancetype = "Debit"
	KREDIT Balancetype = "Kredit"
)
