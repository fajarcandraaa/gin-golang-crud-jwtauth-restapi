package models

import "time"

type Bank_Balance struct {
	ID              int
	Balance         int
	Balance_Achieve int
	Code            string
	Enable          bool
	CreatedAt       time.Time
	UpdatedAt       time.Time
}

type Bank_Balance_History struct {
	ID            int
	BankBalanceId int
	BalanceBefore int
	BalanceAfter  int
	Activity      string
	Type          Balancetype
	Ip            string
	Location      string
	UserAgent     string
	Author        string
	CreatedAt     time.Time
	UpdatedAt     time.Time
}
