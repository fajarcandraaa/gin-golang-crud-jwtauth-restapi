# CRUD GoLang REST API with Gin, GORM, & JWTAuth

![CRUD GoLang REST API with Gin, GORM, & JWTAuth](https://dev-yakuza.posstree.com/assets/images/category/golang/gin/background.jpg)

We will learn about implementing CRUD using Golang REST API  and Gorilla Mux for routing requests, GORM to access the database, and we can use PostgreSQL or MySQL as the database provider.

## Topics :
- Setting up the Golang Project
- Defining the Product Entity
- Connecting to the database
- Routing
- Implementing CRUD in Golang Rest API
	 - Create
	 - Get By ID
	 - Get All
	 - Update
	 - Delete
- Implementing login with jwtAuth

