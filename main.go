package main

import (
	"gin-golang-restapi/auth"
	db "gin-golang-restapi/helpers"
	"gin-golang-restapi/models"
	"gin-golang-restapi/src"

	"github.com/gin-contrib/location"
	"github.com/gin-gonic/gin"
)

// var db *gorm.DB

func main() {
	db := db.ConnectDatabase()
	db.AutoMigrate(
		&models.User{},
		&models.User_Balance{},
		&models.User_Balance_History{},
		&models.Bank_Balance{},
		&models.Bank_Balance_History{},
	)
	router := gin.Default()
	router.Use(location.Default())
	router.POST("/login", src.LoginUserService)

	// router.GET("/", func(c *gin.Context) {
	// 	c.JSON(http.StatusOK, gin.H{
	// 		"name": "Fajar Candra",
	// 	})
	// })

	v1 := router.Group("/v1", auth.AuthorizeJWT())
	v1.POST("/logout", src.LogoutService)
	v1.POST("/users", src.RegisterUsers)

	v1.POST("/adduserbalance", src.AddBalanceUser)
	v1.PUT("/updateuserbalance", src.UpdateUserBalance)

	router.Run(":8888")
}
