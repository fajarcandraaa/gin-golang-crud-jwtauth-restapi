package src

import (
	"fmt"
	db "gin-golang-restapi/helpers"
	"gin-golang-restapi/models"
	"gin-golang-restapi/repository"
	"gin-golang-restapi/repository/middleware"
	"net/http"
	"strconv"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-contrib/location"
	"github.com/gin-gonic/gin"
)

func AddBalanceUser(c *gin.Context) {
	db := db.ConnectDatabase()
	usrbalanceRep := repository.BalanceRepo(db)
	userbalancemodel := models.User_Balance{}
	userbalancehistorymodel := models.User_Balance_History{}

	userbalance_userid := c.PostForm("user_id")
	userbalance_balance := c.PostForm("balance")
	userbalance_balanceacheive := c.PostForm("balance_acheive")
	historyactivity := c.PostForm("activity")
	historytype := c.PostForm("type")
	historyuseragent := c.PostForm("useragent")
	historyauthor := c.PostForm("author")

	userbalancemodel.UserId, _ = strconv.Atoi(userbalance_userid)
	userbalancemodel.Balance = userbalance_balance
	userbalancemodel.BalanceAchieve, _ = strconv.Atoi(userbalance_balanceacheive)

	// cek duplicat data
	if usrbalanceRep.UserBalanceExist(userbalancemodel.UserId) {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  false,
			"messege": "This user had balance account before",
		})
	} else {
		userbalancemodel, err := usrbalanceRep.AddUserBalance(userbalancemodel)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"status":  false,
				"messege": err.Error(),
			})
		} else {
			userbalancehistorymodel.UserBalanceId = userbalancemodel.ID
			userbalancehistorymodel.BalanceBefore = 0
			userbalancehistorymodel.BalanceAfter = 0
			userbalancehistorymodel.Activity = historyactivity
			userbalancehistorymodel.Type = models.Balancetype(fmt.Sprintf(historytype))
			userbalancehistorymodel.Ip = c.ClientIP()
			userbalancehistorymodel.Location = location.Get(c).String()
			userbalancehistorymodel.UserAgent = historyuseragent
			userbalancehistorymodel.Author = historyauthor

			userbalancehistorymodel, err = usrbalanceRep.AddUserBalanceHistory(userbalancehistorymodel)
			if err != nil {
				c.JSON(http.StatusBadRequest, gin.H{
					"status":  false,
					"messege": err.Error(),
				})
			} else {
				c.JSON(http.StatusOK, gin.H{
					"status": true,
					"data":   userbalancemodel,
				})
			}

		}
	}
}

func UpdateUserBalance(c *gin.Context) {
	db := db.ConnectDatabase()
	usrbalanceRep := repository.BalanceRepo(db)
	userbalancehistorymodel := models.User_Balance_History{}
	userbalancehistorymodeltarget := models.User_Balance_History{}

	headerAuth := c.Request.Header["Authorization"]
	tokenarray := strings.Split(headerAuth[0], " ")
	token, _ := middleware.JWTAuthService().ValidateToken(tokenarray[1])
	claims := token.Claims.(jwt.MapClaims)

	usrid_float := claims["ID"].(float64)
	userid := int(usrid_float)
	fmt.Println("userid = ", userid)

	balance, _ := strconv.Atoi(c.PostForm("balance"))
	activity := c.PostForm("activity")
	balancetype := c.PostForm("type")
	idtarget := c.PostForm("target_id")
	idtargetint, _ := strconv.Atoi(idtarget)

	if activity == "BUY" {
		userdatabalance, err := usrbalanceRep.GetUserBalanceByID(userid)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"status":  false,
				"messege": err.Error(),
			})
		} else {
			balanceuser, _ := strconv.Atoi(userdatabalance.Balance)
			balanceachieve := userdatabalance.BalanceAchieve
			updated_balance := strconv.Itoa(balanceuser + balance)
			update_balanceafter, _ := strconv.Atoi(updated_balance)
			update_balancebefore := userdatabalance.History[0].BalanceAfter

			userbalancehistorymodel.UserBalanceId = userdatabalance.ID
			userbalancehistorymodel.BalanceBefore = update_balancebefore
			userbalancehistorymodel.BalanceAfter = update_balanceafter
			userbalancehistorymodel.Activity = activity
			userbalancehistorymodel.Type = models.Balancetype(balancetype)
			userbalancehistorymodel.Ip = c.ClientIP()
			userbalancehistorymodel.Location = location.Get(c).String()
			userbalancehistorymodel.UserAgent = "-"
			userbalancehistorymodel.Author = "-"
			userbalancehistorymodel.TransferTo = "-"
			userbalancehistorymodel.Notes = "-"

			updatesukses, err := usrbalanceRep.UpdateUserBalance(userbalancehistorymodel, userid, updated_balance, balanceachieve, activity, 0, "", 0, userbalancehistorymodeltarget)
			if err != nil {
				c.JSON(http.StatusBadRequest, gin.H{
					"status":  false,
					"messege": err.Error(),
				})
			} else {
				c.JSON(http.StatusOK, gin.H{
					"status":  true,
					"messege": "Sukses menambahkan balance",
					"data":    updatesukses,
					// "fulldata" : userdatabalance,
					// "test": userdatabalance.History[0].BalanceAfter,
					// "data":   userdatabalance.Balance,
					// "total" : updated_balance,
				})
			}
		}
	} else if activity == "TRANSFER" {
		userdatabalance, err := usrbalanceRep.GetUserBalanceByID(userid)
		userdatabalancetarget, err2 := usrbalanceRep.GetUserBalanceByID(idtargetint)
		if err != nil && err2 != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"status":  false,
				"messege": err.Error(),
			})
		} else {
			var note = []string{"Anda sudah melakukan transfer ke", idtarget, "sejumlah", strconv.Itoa(balance)}
			var note2 = []string{"Anda menerima kiriman balance dari", strconv.Itoa(userid), "sejumlah", strconv.Itoa(balance)}
			//start pengirim
			balanceuser, _ := strconv.Atoi(userdatabalance.Balance)
			balanceachieve := userdatabalance.BalanceAchieve
			updated_balance := strconv.Itoa(balanceuser - balance)
			update_balanceafter, _ := strconv.Atoi(updated_balance)
			update_balancebefore := userdatabalance.History[0].BalanceAfter
			userbalancehistorymodel.UserBalanceId = userdatabalance.ID
			userbalancehistorymodel.BalanceBefore = update_balancebefore
			userbalancehistorymodel.BalanceAfter = update_balanceafter
			userbalancehistorymodel.Activity = activity
			userbalancehistorymodel.Type = models.Balancetype(balancetype)
			userbalancehistorymodel.Ip = c.ClientIP()
			userbalancehistorymodel.Location = location.Get(c).String()
			userbalancehistorymodel.UserAgent = "-"
			userbalancehistorymodel.Author = "-"
			userbalancehistorymodel.TransferTo = idtarget
			userbalancehistorymodel.Notes = strings.Join(note, " ")

			//start penerima
			id_usertarget, _ := strconv.Atoi(idtarget)
			balance_target, _ := strconv.Atoi(userdatabalancetarget.Balance)
			balanceachieve_target := userdatabalancetarget.BalanceAchieve
			updatebalance_target := strconv.Itoa(balance_target + balance)
			updatebalanceafter_target, _ := strconv.Atoi(updatebalance_target)
			updatebalancebefore_target := userdatabalancetarget.History[0].BalanceAfter
			userbalancehistorymodeltarget.UserBalanceId = userdatabalancetarget.ID
			userbalancehistorymodeltarget.BalanceBefore = updatebalancebefore_target
			userbalancehistorymodeltarget.BalanceAfter = updatebalanceafter_target
			userbalancehistorymodeltarget.Activity = activity
			userbalancehistorymodeltarget.Type = models.Balancetype(balancetype)
			userbalancehistorymodeltarget.Ip = c.ClientIP()
			userbalancehistorymodeltarget.Location = location.Get(c).String()
			userbalancehistorymodeltarget.UserAgent = "-"
			userbalancehistorymodeltarget.Author = "-"
			userbalancehistorymodeltarget.TransferTo = strconv.Itoa(userid)
			userbalancehistorymodeltarget.Notes = strings.Join(note2, " ")

			updatesukses, err := usrbalanceRep.UpdateUserBalance(userbalancehistorymodel, userid, updated_balance, balanceachieve, activity, id_usertarget, updatebalance_target, balanceachieve_target, userbalancehistorymodeltarget)
			if err != nil {
				c.JSON(http.StatusBadRequest, gin.H{
					"status":  false,
					"messege": err.Error(),
				})
			} else {
				c.JSON(http.StatusOK, gin.H{
					"status":  true,
					"messege": "Sukses melakukan transfer balance",
					"data":    updatesukses,
				})
			}
		}
	} else if activity == "USED" {
		userdatabalance, err := usrbalanceRep.GetUserBalanceByID(userid)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"status":  false,
				"messege": err.Error(),
			})
		} else {
			balanceuser, _ := strconv.Atoi(userdatabalance.Balance)
			balanceachieve := userdatabalance.BalanceAchieve
			update_balancearchieve := balanceachieve + balanceuser
			updated_balance := strconv.Itoa(balanceuser - balance)
			update_balanceafter, _ := strconv.Atoi(updated_balance)
			update_balancebefore := userdatabalance.History[0].BalanceAfter

			userbalancehistorymodel.UserBalanceId = userdatabalance.ID
			userbalancehistorymodel.BalanceBefore = update_balancebefore
			userbalancehistorymodel.BalanceAfter = update_balanceafter
			userbalancehistorymodel.Activity = activity
			userbalancehistorymodel.Type = models.Balancetype(balancetype)
			userbalancehistorymodel.Ip = c.ClientIP()
			userbalancehistorymodel.Location = location.Get(c).String()
			userbalancehistorymodel.UserAgent = "-"
			userbalancehistorymodel.Author = "-"
			userbalancehistorymodel.TransferTo = "-"
			userbalancehistorymodel.Notes = "-"

			updatesukses, err := usrbalanceRep.UpdateUserBalance(userbalancehistorymodel, userid, updated_balance, update_balancearchieve, activity, 0, "", 0, userbalancehistorymodeltarget)
			if err != nil {
				c.JSON(http.StatusBadRequest, gin.H{
					"status":  false,
					"messege": err.Error(),
				})
			} else {
				c.JSON(http.StatusOK, gin.H{
					"status":  true,
					"messege": "Sukses menggunakan balance",
					"data":    updatesukses,
				})
			}
		}
	}

}
