package src

import (
	"gin-golang-restapi/helpers"
	db "gin-golang-restapi/helpers"
	"gin-golang-restapi/models"
	"gin-golang-restapi/repository"
	"gin-golang-restapi/repository/middleware"
	"net/http"

	"github.com/gin-gonic/gin"
)

func RegisterUsers(c *gin.Context) {
	var userRegister models.User
	db := db.ConnectDatabase()
	userRep := repository.UserRepo(db)
	usermodel := models.User{}

	err := c.ShouldBindJSON(&userRegister)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  false,
			"messege": err.Error(),
		})
		return
	}

	usermodel.Email = userRegister.Email
	usermodel.Username = userRegister.Username
	usermodel.Password, _ = helpers.HashPassword(userRegister.Password)

	// cek duplikat data
	if userRep.ExistUser(userRegister.Username, userRegister.Email) {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  false,
			"messege": "Username or email arleady exist",
		})
	} else {
		usermodel, err = userRep.RegistUser(usermodel)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"status":  false,
				"messege": err.Error(),
			})
		} else {
			c.JSON(http.StatusOK, gin.H{
				"status": true,
				"data":   usermodel,
			})
		}
	}

}

func LoginUserService(c *gin.Context) {
	db := db.ConnectDatabase()
	userRep := repository.UserRepo(db)
	usrname := c.PostForm("username")
	psswd := c.PostForm("password")
	existuser, _ := userRep.GetByUsername(usrname)
	userIDexist := existuser.ID
	userdata, _ := userRep.GetByID(userIDexist)

	var login_service repository.LoginService = repository.StaticLoginService(usrname, psswd, db)
	if login_service == nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  false,
			"messege": "Please check your username or password",
		})
	} else {
		var jwt_service middleware.JWTService = middleware.JWTAuthService()
		var login_controller repository.LoginController = repository.LoginHandler(login_service, jwt_service)
		token := login_controller.Login(c, userIDexist)
		if token != "" {
			err := userRep.UpdateToken(usrname, token)
			if err != true {
				c.JSON(http.StatusBadRequest, gin.H{
					"status":  false,
					"messege": "Failed update token",
				})
			} else {
				c.JSON(http.StatusOK, gin.H{
					"status": true,
					"token":  token,
					"data":   userdata,
				})
			}
		} else {
			c.JSON(http.StatusUnauthorized, token)
		}
	}
}

func LogoutService(c *gin.Context) {
	db := db.ConnectDatabase()
	userRep := repository.UserRepo(db)
	usrname := c.PostForm("username")

	err := userRep.DestroyToken(usrname)
	if err != true {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  false,
			"messege": "Logout failed, please contact your administrator",
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"status":  true,
			"messege": "Successfully logout",
		})
	}

}
