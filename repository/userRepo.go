package repository

import (
	"gin-golang-restapi/models"

	"gorm.io/gorm"
)

type UserRepository interface {
	GetUser() ([]models.User, error)
	GetByID(ID int) (models.User, error)
	GetByUsername(Username string) (models.User, error)
	RegistUser(user models.User) (models.User, error)
	UpdateUser(user models.User) (models.User, error)
	DeleteUser(ID int) (models.User, error)

	ExistUser(usrnm string, email string) bool
	ExistToken(usrnm string, token string) bool
	UpdateToken(usrnm string, token string) bool
	DestroyToken(usrnm string) bool
}

type userrepository struct {
	db *gorm.DB
}

func UserRepo(db *gorm.DB) *userrepository {
	return &userrepository{db}
}

func (r *userrepository) GetUser() ([]models.User, error) {
	var users []models.User

	err := r.db.Find(&users).Error

	return users, err
}

func (r *userrepository) GetByID(ID int) (models.User, error) {
	var users models.User

	err := r.db.Preload("Detail").Select("id", "username", "email").Find(&users, ID).Error

	return users, err
}

func (r *userrepository) GetByUsername(Username string) (models.User, error) {
	var users models.User

	err := r.db.Where("username = ?", Username).First(&users).Error

	return users, err
}

func (r *userrepository) RegistUser(user models.User) (models.User, error) {
	err := r.db.Create(&user).Error

	return user, err
}

func (r *userrepository) UpdateUser(user models.User) (models.User, error) {

	err := r.db.Updates(&user).Error

	return user, err
}

func (r *userrepository) DeleteUser(ID int) (models.User, error) {
	err := r.db.Delete(&models.User{}, ID).Error

	return models.User{}, err
}

// UNTUK KEPERLUAN AUTENTIKASI USER
func (r *userrepository) ExistUser(usrnm string, email string) bool {
	var users models.User

	err := r.db.Where("username = ?", usrnm).Or("email = ?", email).First(&users).Error
	if err != nil {
		return false
	} else {
		return true
	}
}

func (r *userrepository) ExistToken(usrnm string, token string) bool {
	var users models.User

	err := r.db.Where("username = ? AND token = ?", usrnm, token).First(&users).Error
	if err != nil {
		return false
	} else {
		return true
	}
}

func (r *userrepository) UpdateToken(usrnm string, token string) bool {
	var userdata models.User
	err := r.db.Model(&userdata).Where("username = ?", usrnm).Update("token", token).Error
	if err != nil {
		return false
	} else {
		return true
	}
}

func (r *userrepository) DestroyToken(usrnm string) bool {
	var userdata models.User
	err := r.db.Model(&userdata).Where("username = ?", usrnm).Update("token", "-").Error
	if err != nil {
		return false
	} else {
		return true
	}
}
