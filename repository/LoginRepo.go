package repository

import (
	"gin-golang-restapi/helpers"
	"gin-golang-restapi/models"
	"gin-golang-restapi/repository/middleware"

	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

type LoginService interface {
	LogInUser(username string, password string) bool
}

type loginInformation struct {
	username string
	password string
}

func StaticLoginService(usrnm string, pass string, db *gorm.DB) LoginService {
	var userVerify models.User

	err := db.Where("username = ?", usrnm).Find(&userVerify).Error
	if err != nil {
		return nil

	}
	cekpassword := helpers.CheckPasswordHash(pass, userVerify.Password)
	if cekpassword {
		return &loginInformation{
			username: userVerify.Username,
			password: pass,
		}
	} else {
		return nil
	}

}

func (info *loginInformation) LogInUser(username string, password string) bool {
	return info.username == username && info.password == password
}

// =========================================================

type LoginController interface {
	Login(c *gin.Context, ID int) string
}

type loginController struct {
	loginSer LoginService
	jwtser   middleware.JWTService
}

func LoginHandler(loginservice LoginService, jwrservice middleware.JWTService) LoginController {
	return &loginController{
		loginSer: loginservice,
		jwtser:   jwrservice,
	}
}

func (controller *loginController) Login(ctx *gin.Context, ID int) string {
	var credential middleware.LoginCredentials
	err := ctx.ShouldBind(&credential)
	if err != nil {
		return "no data found"
	}
	isUserAuthenticated := controller.loginSer.LogInUser(credential.Username, credential.Password)
	if isUserAuthenticated {
		return controller.jwtser.GenerateToken(credential.Username, true, ID)

	}
	return ""
}
