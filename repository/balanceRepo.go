package repository

import (
	"gin-golang-restapi/models"

	"gorm.io/gorm"
)

type BalanceRepository interface {
	AddUserBalance(userbalance models.User_Balance) (models.User_Balance, error)
	AddUserBalanceHistory(userbalancehistory models.User_Balance_History) (models.User_Balance_History, error)
	UpdateUserBalance(userbalancehistory models.User_Balance_History, IDUser int, bal string, balArc int, acction string, IDTarget int, bal2 string, balArc2 int, userbalancehistorytarget models.User_Balance_History) (models.User_Balance, error)
	GetUserBalanceByID(IDUser int) (models.User_Balance, error)
	UserBalanceExist(IDUser int) bool
}

type balancerepository struct {
	db *gorm.DB
}

func BalanceRepo(db *gorm.DB) *balancerepository {
	return &balancerepository{db}
}

func (r *balancerepository) AddUserBalance(userbalance models.User_Balance) (models.User_Balance, error) {
	err := r.db.Create(&userbalance).Error

	return userbalance, err
}

func (r *balancerepository) AddUserBalanceHistory(userbalancehistory models.User_Balance_History) (models.User_Balance_History, error) {
	err := r.db.Create(&userbalancehistory).Error

	return userbalancehistory, err
}

func (r *balancerepository) UserBalanceExist(IDUser int) bool {
	var userbalance models.User_Balance

	err := r.db.Where("user_id = ?", IDUser).First(&userbalance).Error

	if err != nil {
		return false
	} else {
		return true
	}
}

func (r *balancerepository) UpdateUserBalance(userbalancehistory models.User_Balance_History, IDUser int, bal string, balArc int, action string, IDTarget int, bal2 string, balArc2 int, userbalancehistorytarget models.User_Balance_History) (models.User_Balance, error) {
	var userbalance models.User_Balance
	var targetuser models.User_Balance

	if action != "TRANSFER" {
		err := r.db.Model(&userbalance).Where("user_id = ?", IDUser).Updates(models.User_Balance{Balance: bal, BalanceAchieve: balArc}).Error
		if err != nil {
			return userbalance, err
		} else {
			err := r.db.Create(&userbalancehistory).Error
			return userbalance, err
		}
	} else {
		err := r.db.Model(&userbalance).Where("user_id = ?", IDUser).Updates(models.User_Balance{Balance: bal, BalanceAchieve: balArc}).Error
		if err != nil {
			return userbalance, err
		} else {
			err := r.db.Create(&userbalancehistory).Error
			if err != nil {
				return userbalance, err
			} else {
				err2 := r.db.Model(&targetuser).Where("user_id = ?", IDTarget).Updates(models.User_Balance{Balance: bal2, BalanceAchieve: balArc2}).Error
				if err2 != nil {
					return userbalance, err
				} else {
					err2 := r.db.Create(&userbalancehistorytarget).Error
					return userbalance, err2
				}
			}
		}
	}
}

func (r *balancerepository) GetUserBalanceByID(IDUser int) (models.User_Balance, error) {
	var userbal models.User_Balance

	err := r.db.Preload("History", func(db *gorm.DB) *gorm.DB {
		return db.Order("user_balance_histories.id DESC")
	}).Where("user_id = ?", IDUser).First(&userbal).Error

	return userbal, err
}
