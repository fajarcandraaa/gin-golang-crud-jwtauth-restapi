package middleware

//Login credential
type LoginCredentials struct {
	Username string `json:"username" form:"username""`
	Password string `json:"password" form:"password"`
}
