package helpers

import (
	//    "github.com/gin-gonic/gin"
	"log"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// ConnectDatabase : Ini Untuk menghubungkan database Server
func ConnectDatabase() *gorm.DB {

	dsn := "root:@tcp(127.0.0.1:3306)/gin-golang-restapi?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Fatal("DB Connection Error")
	}

	return db
}
